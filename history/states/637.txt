state={
	id=637
	name="STATE_637"
	resources={
		oil=0
		aluminium=0
		rubber=0
		tungsten=0
		steel=0
		chromium=0
		grain=0
	}
	state_category = town
	history={
		owner = PDA
		#victory_points = { PROV 5 }
		buildings = {
			infrastructure = 2
			#dockyard = 0
			6420 = {
				naval_base = 2
			}
			industrial_complex = 1
			arms_factory = 1
			air_base = 0
			anti_air_building = 0
		}
		add_core_of = PDA
	}
	provinces={
		556 6388 6390 6407 6420 6430 6444 
	}
	manpower=100000
	buildings_max_level_factor=1.000
}
