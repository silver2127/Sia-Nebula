
state={
	id=164
	name="STATE_164"

	history={
		owner = NWA
		buildings = {
			infrastructure = 2
			industrial_complex = 1
			arms_factory = 1
			air_base = 1
			anti_air_building = 0

		}
		add_core_of = NWA

	}

	provinces={
		152 784 916 2134 2922 4643 4655 4664 4667 4692 4724 
	}
	manpower=2
	buildings_max_level_factor=1.000
	state_category=large_town
	impassable = yes
}
