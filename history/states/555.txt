state={
	id=555
	name="STATE_555"
	resources={
		oil=0
		aluminium=0
		rubber=0
		tungsten=0
		steel=0
		chromium=0
		grain=0
	}
	state_category = town
	history={
		owner = OIA
		#victory_points = { PROV 5 }
		buildings = {
			infrastructure = 2
			#dockyard = 0
			#PROV = {
			#	naval_base = 1
			#}
			industrial_complex = 1
			arms_factory = 1
			air_base = 0
			anti_air_building = 0
		}
		add_core_of = OIA
		add_core_of = TST
	}
	provinces={
		1659 2799 5008 5037 5073 5074 
	}
	manpower=25000
	buildings_max_level_factor=1.000
}
